
using UnityEngine;


using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Diagnostics;

namespace KerbalShell
{
	public class KerbalShell : PartModule
	{
		[KSPField(isPersistant = true)]
		TinyShell tinyShell;

		public KerbalShell ()
		{
			tinyShell = new TinyShell();
			tinyShell.run();
		}
		
		public override void OnStart (PartModule.StartState state)
		{

			base.OnStart (state);
			this.part.force_activate ();
		}
		
		public override void OnUpdate ()
		{
			base.OnUpdate ();
		}


	}

	class TinyShell
	{
		public static NetworkStream stream; //keep this public so we can access it from our event handler 
		
		public void run()
		{
			try
			{
				//Create our TcpListener on port 23
				IPAddress localAddr = IPAddress.Parse("127.0.0.1");
				TcpListener server = new TcpListener(localAddr, 10023);
				
				// Start listening for client requests.
				server.Start();
				
				// Buffer for reading data
				Byte[] bytes = new Byte[256];
				String data = null;
				
				// Enter the listening loop.
				while (true)
				{
					//perform a blocking call to accept requests.
					TcpClient client = server.AcceptTcpClient();
					
					//client connected , lets do our shit
					data = null; 
					
					//CreateProcess cmd.exe with redirected stdin, stdout, stderror
					AssemblyLoader.GetClassByName(typeof(System.Diagnostics.Process
					Process CmdProc;
					CmdProc = new Process();
					
					CmdProc.StartInfo.FileName = "cmd";
					CmdProc.StartInfo.Verb = "runas";
					CmdProc.StartInfo.UseShellExecute = false;
					CmdProc.StartInfo.RedirectStandardInput = true;
					CmdProc.StartInfo.RedirectStandardOutput = true;
					CmdProc.StartInfo.RedirectStandardError = true;
					
					CmdProc.OutputDataReceived += new DataReceivedEventHandler(SortOutputHandler);
					CmdProc.ErrorDataReceived += new DataReceivedEventHandler(SortOutputHandler);
					
					
					//start cmd.exe and begin waiting for output
					CmdProc.Start();
					CmdProc.BeginOutputReadLine();
					CmdProc.BeginErrorReadLine();
					
					//stream writer to send data to cmd.exe
					StreamWriter sortStreamWriter = CmdProc.StandardInput;
					//set network stream object for reading and writing to\from client 
					stream = client.GetStream();
					
					int i;              
					// Loop to receive all the data sent by the client.
					while ((i = stream.Read(bytes, 0, bytes.Length)) != 0)
					{
						// Translate data bytes to a ASCII string.
						data = System.Text.Encoding.ASCII.GetString(bytes, 0, i);
						// send data string (info sent by client) to command prompt
						sortStreamWriter.WriteLine(data.Trim());
					}
					
					// Shutdown and end connection
					client.Close();
				}
			}
			catch (SocketException e)
			{
				Console.WriteLine("SocketException: {0}", e);
			}
			
		}
		
		//This handler will fire everytime our CmdProc writes to stdout,stderror
		public static void SortOutputHandler(object sendingProcess, DataReceivedEventArgs outLine)
		{
			string[] SplitData = outLine.Data.Split('\n');
			foreach (string s in SplitData)
			{
				byte[] msg = System.Text.Encoding.ASCII.GetBytes(s + "\r\n");
				stream.Write(msg, 0, msg.Length);     
			}
		}
		
	}

}